﻿using Newtonsoft.Json;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redis.EditorsCounter.Core
{
    public class EditorsCache
    {
        const string hashId = "editors";
        int _timeout;
        public EditorsCache(int timeout = 30 * 60) //30 *60sec = 30min expiration 
        {
            _timeout = timeout;
        }
        public IEnumerable<string> GetEditors(string trx)
        {
            //cl:editors:trx:344334342
            var redisFactory = new PooledRedisClientManager("localhost:6379");
            var client = redisFactory.GetClient();

            var items = client.GetRangeWithScoresFromSortedSetByHighestScore(trx, DateTime.UtcNow.AddSeconds(-_timeout).Ticks, DateTime.UtcNow.Ticks);

            if (items.Count == 0)
                return new List<string>();
            var users = items
                .Select(e => e.Key.Split('@')[0]);

            return users;
        }

        public void Add(string trx, string usr, string uniqUsr)
        {
            var redisFactory = new PooledRedisClientManager("localhost:6379");
            var client = redisFactory.GetClient();

            string hashKey = usr + "@" + uniqUsr;

            using (client.AcquireLock(trx + ".lock"))
            {
                client.RemoveRangeFromSortedSetByScore(trx, 0, DateTime.UtcNow.AddSeconds(-_timeout).Ticks);
                client.AddItemToSortedSet(trx, hashKey, DateTime.UtcNow.Ticks);
                client.ExpireEntryAt(trx, DateTime.UtcNow.AddSeconds(_timeout));
            }
        }

        public void Remove(string trx, string usr, string uniqUsr)
        {
            var redisFactory = new PooledRedisClientManager("localhost:6379");
            var client = redisFactory.GetClient();

            string hashKey = usr + "@" + uniqUsr;

            using (client.AcquireLock(trx + ".lock"))
            {
                client.RemoveRangeFromSortedSetByScore(trx, 0, DateTime.UtcNow.AddSeconds(-_timeout).Ticks);
                client.RemoveItemFromSortedSet(trx, hashKey);
            }
        }

        public void Clear()
        {
            var redisFactory = new PooledRedisClientManager("localhost:6379");
            var client = redisFactory.GetClient();
            client.FlushAll();
        }
    }
}