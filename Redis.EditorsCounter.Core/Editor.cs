﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis.EditorsCounter.Core
{
    public class Editor
    {
        public string UserName { get; set; }
        public string UniqueId { get; set; } //uniqly identity open tab/browser
        public DateTime ModifyTime { get; set; }

    }
}
