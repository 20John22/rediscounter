﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading;
using Redis.EditorsCounter.Core;
using System.Linq;

namespace Redis.EditorsCounter.Core.Test
{
    [TestClass]
    public class EditorsCounterTest
    {
        [TestInitialize]
        public void Test()
        {
            EditorsCache p = new EditorsCache();
            p.Clear();
        }
        [TestMethod]
        public void Returns_Empty_List_When_No_Editors()
        {
            //Arrange
            string trx = "trx:3432";
            EditorsCache p = new EditorsCache();

            //Act
            IEnumerable<string> editors = p.GetEditors(trx);

            //Arrange
            Assert.AreEqual(0, editors.Count());
        }

        [TestMethod]
        public void Can_Add_Editor()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache();

            //Act
            p.Add(trx, usr, uniqUsr);

            //Arrange
            IEnumerable<string> editors = p.GetEditors(trx);

            Assert.AreEqual(1, editors.Count());
        }

        [TestMethod]
        public void Can_Add_Same_Editor_With_Diffrent_Unique_Id()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache();

            //Act
            p.Add(trx, usr, uniqUsr);
            p.Add(trx, usr, uniqUsr + "ext");

            //Arrange
            IEnumerable<string> editors = p.GetEditors(trx);

            Assert.AreEqual(2, editors.Count());
        }

        [TestMethod]
        public void Can_Override_Same_Editor()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache();

            //Act
            p.Add(trx, usr, uniqUsr);
            p.Add(trx, usr, uniqUsr);

            //Arrange
            IEnumerable<string> editors = p.GetEditors(trx);

            Assert.AreEqual(1, editors.Count());
        }

        [TestMethod]
        public void Can_Expire_Editor_Trx()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache(1);

            //Act
            p.Add(trx, usr, uniqUsr);
            Thread.Sleep(1200);

            //Arrange
            IEnumerable<string> editors = p.GetEditors(trx);

            Assert.AreEqual(0, editors.Count());
        }

        [TestMethod]
        public void Can_Expire_Editor_Trx2()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache(10);

            //Act
            p.Add(trx, usr, uniqUsr);
            Thread.Sleep(5000);

            p.Add(trx, usr, uniqUsr + "1");
            Thread.Sleep(5100);

            //Assert
            var editors = p.GetEditors(trx);
            Assert.AreEqual(1, editors.Count());
        }

    //TODO: add test to check remove expired items when adding new item

        [TestMethod]
        public void Can_Remove_Editor()
        {
            //Arrange
            string trx = "trx:3432";
            string usr = "test1";
            string uniqUsr = "untest1";
            EditorsCache p = new EditorsCache();

            p.Add(trx, usr, uniqUsr);

            //Act
            p.Remove(trx, usr, uniqUsr);

            //Arrange
            IEnumerable<string> editors = p.GetEditors(trx);

            Assert.AreEqual(0, editors.Count());
        }
    }
}
